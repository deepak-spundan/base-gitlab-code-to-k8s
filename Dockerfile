FROM maven:3.6.0-jdk-8 AS build  
COPY . .
RUN pwd
RUN mvn clean package 
RUN ls ./target


FROM openjdk:9  
COPY --from=build /target/spring-boot-rest-example-0.5.0.war /usr/app/spring-boot-rest-example-0.5.0.war  
EXPOSE 8091  
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=test","/usr/app/spring-boot-rest-example-0.5.0.war"] 
